import pytest

# =====================================
# 範例，Fixture 作為參數範例，主程式
# =====================================
class MyError(Exception):
    pass

class Wallet(object):
    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        if self.balance < amount:
            raise MyError("Not enough available to spend {}".format(amount))
        self.balance -= amount


    def add_cash(self, amount):
        self.balance += amount

# =====================================
# 範例，Fixture 作為參數範例，測試程式
# =====================================

# 建立 fixture，用來初始化 wallet-object
# 建立 amount = 0 的 wallet-object
@pytest.fixture
def empty_wallet():
    ''' Return wallet-object with initial amount = 0 '''
    return Wallet(0)

# 建立 fixture，用來初始化 wallet-object，
# 建立 amount = 100 的 wallet-object
@pytest.fixture
def wallet():
    ''' Return wallet-object with initial amount = 100 '''
    return Wallet(100)

# 測試類的初始屬性
def test_defau_initial_amount(empty_wallet, wallet):
    assert empty_wallet.balance == 0
    assert wallet.balance == 100

# 單獨測試成員函數spend_cash()的功能
def test_spend_cash(empty_wallet, wallet):
    # 檢查是否拋出異常
    with pytest.raises(MyError):
        empty_wallet.spend_cash(100)

    wallet.spend_cash(100)
    assert wallet.balance == 0

# 單獨測試成員函數add_cash()的功能
def test_add_cash(empty_wallet, wallet):
    empty_wallet.add_cash(100)
    assert empty_wallet.balance == 100
    wallet.add_cash(100)
    assert wallet.balance == 200

# 測試類的成員函數的綜合行為
def test_wallet_behavior(empty_wallet, wallet):
    # 注意，在此範例中 add_cash() 和 spend_cash()都沒有 return，
    # 必須透過屬性 balance 來驗證結果

    empty_wallet.add_cash(100)
    assert empty_wallet.balance == 100
    wallet.add_cash(100)
    assert wallet.balance == 200

    # 注意，因為寫在同一個函數內，wallet-object 不會重新初始化
    # 若要重新初始化，應該寫成獨立的測試函數
    empty_wallet.spend_cash(100)
    assert empty_wallet.balance == 0
    wallet.spend_cash(200)
    assert wallet.balance == 0




