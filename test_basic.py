# =====================================
# 將測試和主程式分開的寫法範例
# test_basic.py 主要測試 basic.py 中的函數
# =====================================

import pytest
from basic import *

# 驗證 add() 的正常用例
def test_add():
    assert add(1, 4) == 5
    assert add(1, 3) == 4
    assert add(2, 3) == 5

# 檢查程式是否結束
def test_raise_exit():
    with pytest.raises(SystemExit):
        raise_exit()

# 檢查是否有某個屬性
def test_string():
    result = string("123")

    # 若引數是字串，會有 split 的屬性
    assert hasattr(result, "split")

    result = string(123)
    # 若引數是字串，不會有 split 的屬性
    assert not hasattr(result, "split")

# 該測項不會被執行
@pytest.mark.skip
def test_skip():
    assert 1 == 2

# skipif 需要提供 reason 的參數
@pytest.mark.skipif(True, reason="I dont know why either")
def test_skipif():
    assert 1 == 2
