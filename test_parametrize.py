import pytest

# =====================================
# 範例，@pytest.mark.parametrize使用範例
# 主程式
# =====================================
class MyError(Exception):
    pass

class Wallet(object):
    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        if self.balance < amount:
            raise MyError("Not enough available to spend {}".format(amount))
        self.balance -= amount


    def add_cash(self, amount):
        self.balance += amount


# =====================================
# 範例，@pytest.mark.parametrize使用範例
# 測試程式
# =====================================

# 將多組測試數值參數化
# @pytest.mark.parametrize("變數名1,變數名2,變數名3, ...", 對應變數的數值列表)
@pytest.mark.parametrize("earn,spend,expect", [(30, 10, 20), (100, 50, 50)])
def test_transactions(earn, spend, expect):
    wallet = Wallet(0)
    wallet.add_cash(earn)
    wallet.spend_cash(spend)
    assert wallet.balance == expect

# 結合 fixture 和 試數值參數化試數值參數化
# 建立 fixture
@pytest.fixture
def myWallet():
    return Wallet(0)

# 參數化測試數值，並帶入測試函數中
# pytest.param(值1, 值2, 值3,...., mark=條件或預期結果)，
# 通常與@pytest.mark.parametrize搭配使用，用來驗證fail
# pytest.param 的使用，請參考 https://docs.pytest.org/en/latest/reference.html?highlight=.param#pytest-param
@pytest.mark.parametrize("earn,spend,expect", [(30, 10, 20), pytest.param(30, 10, 30, marks=pytest.mark.xfail)])
def test_transactions(myWallet, earn, spend, expect):
    myWallet.add_cash(earn)
    myWallet.spend_cash(spend)
    assert myWallet.balance == expect
