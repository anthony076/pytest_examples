import pytest

class TestSample:
    @pytest.fixture()
    def countA(self):
        return 10

    def test_countA(self, countA):
        assert countA == 10

    # 使用外部的 fixture (countB 定義在 ext_fixture_test.py)
    # 注意，外部 fixture 的檔名必須叫 conftest.py，外部的 fixture 才能被找到
    def test_countB(self, countB):
        assert countB == 100
