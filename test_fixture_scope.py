import pytest

class MyError(Exception):
    pass

class Wallet(object):
    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        if self.balance < amount:
            raise MyError("Not enough available to spend {}".format(amount))
        self.balance -= amount

    def add_cash(self, amount):
        self.balance += amount

@pytest.fixture(scope="module")
def myWallet():
    return Wallet(100)              # 初始錢包 100

def test_spend(myWallet):
    myWallet.add_cash(100)          # 錢包 + 100
    assert myWallet.balance == 200  # 測試現有錢包為200，PASS

def test_spend_again(myWallet):
    # 若 fixture 的 scope="function"，調用 fixture 時會重新初始化，現有錢包應為 100，測項結果為 FAIL
    # 若 fixture 的 scope="session"，調用 fixture 時不會重新初始化，現有錢包應為 200，測項結果為 PASS
    assert myWallet.balance == 200  # 測試現有錢包為200
