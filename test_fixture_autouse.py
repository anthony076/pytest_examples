import pytest

@pytest.fixture(autouse=True)
def my_fixture():
    print("\nI'm the fixture")
    return 10

def test_my_fixture():
    print("I'm the test")

class Test:
    def test1(self, my_fixture):
        print("I'm the test 1")
        assert my_fixture == 10

    def test2(self):
        print("I'm the test 2")