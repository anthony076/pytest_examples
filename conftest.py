import pytest

# 透過 scope 參數，讓fixture 可以被不同的目標使用
# 注意，外部 fixture 的檔名必須叫 conftest.py，外部的 fixture 才能被找到
@pytest.fixture(scope="session")
def countB():
    return 100