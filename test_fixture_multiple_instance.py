import pytest

# =====================================
# 透過 params 參數可產生多個 fixture 實例
# =====================================
@pytest.fixture(params=[2, 3, 4])
def A(request):   # request 是使用 params參數時，預設的引數名，不能換
    # 透過 request.param 存取帶進來的值
    return request.param + 1

# fixture A 具有三個實例
def test_four(A):
    assert A < 50

