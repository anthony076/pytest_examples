import pytest

ERROR1 = ' --- Error : value < 5! ---'
ERROR2 = ' --- Error : value > 10! ---'

class MyError(Exception):
    def __init__(self, m):
        self.m = m

    def __str__(self):
        return self.m

# 被測試的函數
def foo(i):
    if i < 5:
        raise MyError(ERROR1)
    elif i > 10:
        raise MyError(ERROR2)
    else:
        return i

# ===== 驗證 foo() 是否有引發自定義的錯誤 =====
# 利用以下兩種方法，若有正確拋出 Execption，則不會報 test fail
# 方法一，利用 with pytest.raises() 函數
def test_error_methodA():
    with pytest.raises(MyError):
        foo(4)

# 方法二，利用 ＠pytest.mark.xfail 裝飾器
@pytest.mark.xfail
def test_error_methodB():
    foo(4)
